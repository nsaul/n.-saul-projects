﻿/// <reference path="../jquery-3.1.1.js" />
/// <reference path="../jquery.validate.js" />

$(document).ready(function () {
	$(".loginForm").validate({
		
		rules: {
			email: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			email: {
				required: "Email is required",
			},
			password: {
				requeired: "Password is required",
			}
		},
 		errorClass: "error", 
        validClass: "valid" 
	});
});

$(document).ready(function () {
	$(".registerForm").validate({

		rules: {
			email: {
				required: true,
				governmentEmail: true,
				emailTrue: true
				},
			password: {
				required: true,
				minlength: 8,
			},
			confirmPassword: {
				required: true,
				equalTo: '#password'
			}
		},
		messages: {
			email: {
				required: "Please input an email address",
				endsWith: "Email must end in '.gov'"
			},
			password: {
				required: "Please input a password",
				minlength: "Password must be at least 8 characters long"
			},
			confirmPassword: {
				equalTo: "Passwords must match"
				
			}
		},
		
	});
});
$(document).ready(function () {
	$("#checkout").validate({

		rules: {
			BillingAddress1: {
				required: true
			},
			BillingCity: {
				required: true
			},
			BillingState: {
				required: true
			},
			BillingPostalCode: {
				required: true,
				minlength: 5,
				maxlength: 5
			},
			ShippingAddress1: {
				required: true
			},
			ShippingCity: {
				required: true
			},
			ShippingState: {
				required: true
			},
			ShippingPostalCode: {
				required: true,
				minlength: 5,
				maxlength: 5
			}
		},
		messages: {
			BillingAddress1: {
				required: "Please enter a Billing Address"
			},
			BillingCity: {
				required: "Please enter a city"
			},
			BillingState:{
				required: "Please enter a valid state"
			},
			BillingPostalCode: {
				required: "Please enter a valid postal code",
				minlength: "The postal code must contain 5 numbers",
				maxlength: "The postal code must contain 5 numbers"
			},
			ShippingAddress1: {
				required: "Please enter a Billing Address"
			},
			ShippingCity: {
				required: "Please enter a city"
			},
			ShippingState:{
				required: "Please enter a valid state"
			},
			ShippingPostalCode: {
				required: "Please enter a valid postal code",
				minlength: "The postal code must contain 5 numbers",
				maxlength: "The postal code must contain 5 numbers"
			}
		},
		
	});
});
$(document).ready(function () {
	$("#payment-info").validate({

		rules: {
			NameOnCard: {
				required: true
			},
			CreditCardNumber: {
				required: true,
				minlength: 16,
				maxlength: 16
			},
			ExpirationDate: {
				required: true,
				minlength: 5,
				maxlength: 5
			}
		},
		messages: {
			NameOnCard: {
				required: "Please enter the name on your credit card"
			},
			CreditCardNumber: {
				required: "Please enter a credit card number",
				minlength: "Please enter a valid credit card number",
				maxlength: "Please enter a valid credit card number"
			},
			ExpirationDate: {
				required: "Please enter an exipiration date",
				minlength: "Please enter a date xx/xx",
				maxlength: "Please enter a date xx/xx"
			}
		}
	});
});
$.validator.addMethod("governmentEmail", function (value, index) { 
    return value.toLowerCase().endsWith(".gov");  
}, "Please enter a .gov email");
$.validator.addMethod("emailTrue", function (value, index) {
	return value.toLowerCase().contains("@");
}, "Please enter a valid email");
